
import './App.css';
import ComponentName from './ComponentName';

function App() {
  return (
    <div className="App">
      <ComponentName country="Finland" />
    </div>
  );
}

export default App;
